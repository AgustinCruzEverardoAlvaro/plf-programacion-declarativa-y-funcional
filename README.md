 # PROGRAMACION LOGICA Y FUNCIONAL -SCC1019-ISA-20213
![](imagenes/tec.png)
## Lenguajes de programación y algoritmos
# plf-programacion-declarativa-y-funcional
# Programación Declarativa. 
## Descripcion 
 el mapa conceptual trata sobre la programacion declarativa y sus partes que la conforman.
```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  	#FAEBD7
    LineBorder none
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  #00FFFF
    }
}
</style>
title  Programación Declarativa.
*[#green] Programación Declarativa. 
**_ es

***[#red] paradigma de programacion 
****_ se direrencia 
***** de la progrmacion imperativa
******_ esto 
******* que tiene asignaciones y control de memoria 
***[#red] funcional : se centra en las\n funciones matematicas
****_ utiliza el 
***** lenguaje Haskell 
****_ utiliza 
***** funciones matematicas
***** reglas de escritura ejecutarse
******_ como 
******* quitar las asignaciones 
****_ caracteristicas
***** el codigo puede ejecutarse \n en llamadas de secuencias funcionales 
***** no se asignan valores  \n de forma reutilizable secuencias
***** devuelven un resultado ejecutarse
***** no hay variables globales y cambiantes 
***** no manejo de memoria 
***** control detalldo de ejecucion 
****_ tiene 
***** programacion primitiva 
******_ y dejar 
******* tareas tediosas al compilador 
****_ no basada 
***** en el modelo de Bhom Neuman 
******_ y se basa 
******* de los matematicos 
****_ tine 
***** datos de entrada y de salida 
******_ pueden tener 
******* funciones que reciben otras \n funciones

**_  tiene 
***[#red]   evaluacion perezosa
****_ su funcion es 
***** hacer operaciones hasta\n que se necesiten 
****_ que evalua las funciones
*****[#orange] ventajas 
****** definir datos infinitos
****** menor tiempo de progrmacion
****** depuracion mas rapida
****** eliminar ambiguedades en lenguajes \n de alto nivel

*******_ ejemplo 
******** la serie de Fibonaci 

**_ su objetivo 
*** reducir la complejidad de los programas
****_ y el riego
***** de cometer errores haciendo programas 

**_ es  
***[#red] logica
****_ recurre
*****[#orange] a la logica de primer orden
******_ tiene 
******* Predicados 
******* Permite ser mas declarativos 
********_ ejemplo 
********* una relacion factorial de dos\n numeros  
****_ uliza la
*****[#orange] demostracion 
****** logica 
****** automatica
******_ se ocupan 
******* reglas de inferencia
******* axiomas

**_  tiene 
***[#red] se diferencia 
****_ la 
***** parte creativa
******_ es la 
******* idea del algoritmos
********_ para
********* dar solucion a los problemas \n presentados en la programacion

*****[#orange] parte practica 
*****_ que se 
******[#orange] encarga de gestionar la memoria 
******_ creando 
******* una secuencia de ordenes 
********_ para 
********* llegar al resultado esperado 
@endmindmap
```
[Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)
[Refrencia 1](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)
[Referencia )](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

# Lenguaje de Programación Funcional (2015)
## Descripcion 
 el mapa trata sobre que es la programacion Funcional su definicion y como se puede comprender mejor

```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  	#fd9d9d9
    LineBorder none
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  #66ff66
    }
}
</style>
title  Lenguaje de Programación Funcional (2015).
*[#orange] Lenguaje de Programación Funcional
**_ es 
***[#red] un paradigma
****_ que es 
***** es el modelo  de computacion  el  diferentes lenguajes que dotan \n a los lenguajes de programacion de semantica 
******_ basada
******* en el mdelo de bhom neuman 
********_ el cual 
********* define que los progrmas serian una secuanecia de instrucciones \n ejecutadas una tras otra.

**_ basado  en 
***[#red] logica simbolica
**** define lo que es verdad 
****_ para
***** un progrma
******_ respecto 
******* un problema 
********_ sin necesidad
********* de indicarle cual es la secuencia logica

***[#red] lamda calculo
****_ se desarrollo 
***** en la decada de 1930
******_ por
******* Stephen Kleene 
********_ tiene
********* El cálculo lambda tiene una gran influencia\n  sobre los lenguajes funcionales, como Lisp, ML y Haskell.

***[#red] logica combinatoria
****_ que es
***** una variante de lamda calculo
*****_ o
****** puede ser un modelo simplificado del computo
*******_ que en 
******** en el 65
*********_ dijo 
********** Piter Landin : va poder modelar \n un lenguaje de progrmacion 

**_ tiene 
***[#red] consecuencias
****_ como
***** comparar la programacion imperativa \n de la asignacion x= x+2
******_ no significa 
******* lo mismo en lenguaje funcional \n por conceptos matematicos ningun numero que asi mismo le sumes 2 te de el mismo resultado.

**_ utiliza 
***[#red] la recursion 
****_ con 
***** funciones recursivas
******_ por ejemplo 
******* el factorial de un numero 

**_ no utiliza
***[#red] estados
****_ o 
***** asignaciones de variables
******_ por lo
******* todo se hace de una manera distinta

**_ los
***[#red] progrmas 
****_ solo
***** dependeran de los parametros de entrada

**_ ocupa 
***[#red] tranparencia referencial 
****_ esto 
***** quiere decir que no importa \n en oreden en el cual se calcule las operaciones
******_ ejemplo 
******* si se calcula el cuadrado \n de 7 o de 5
********_ siempre tendra 
********* el mismo tipo de entrada es el mismo que sale 

 **_ tiene
 ***[#red] caracteristicas
 ****_ como
 ***** todo gira al rededor de las funciones
 ***** todo son funciones
 ***** ciudadanos de primera clase
 ***** tiene funciones de orden superior
 ***** la sadia de una funcion es una funcion
 ***** puede recibir funciones como entradas

 **_ denomina 
 ***[#red] curryficacion
 ****_ en honor
 ***** a Haskell curry
 ******_ como una
 ******* funcion  
 ********_ que tiene
 ********* varios parametros de entrada \n no solo devuelve una salida.
 **********_ sino 
 *********** diferentes salidas o diferentes funciones
 *********** se conoce aplicacion parcial 

 **_ ocupa la
 ***[#red] Evaluacion perezosa
 ****_ que consiste
 *****  llamada por necesidad es una estrategia de evaluación que retrasa el \n cálculo de una expresión hasta que su valor sea necesario, y que también evita \nrepetir la evaluación en \ncaso de ser necesaria en posteriores ocasiones.

****_  ventajas
***** El incremento en el rendimiento\n al evitar cálculos innecesarios
***** La capacidad de construir\n estructuras de datos potencialmente infinitas
***** La capacidad de definir estructuras de control\n como abstracciones, en lugar de operaciones primitivas.







@endmindmap
```
[Lenguaje de Programación Funcional (2015)](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)

## Autor
> Agustin Cruz Everardo Alvaro 


## Herramientas utilizadas
[Diagrams and flowcharts](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
[PlantUML and GitLab](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
[MindMap](https://plantuml.com/es/mindmap-diagram)

